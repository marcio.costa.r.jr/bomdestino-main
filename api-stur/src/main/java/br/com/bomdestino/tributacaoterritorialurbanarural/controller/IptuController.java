package br.com.bomdestino.tributacaoterritorialurbanarural.controller;

import br.com.bomdestino.tributacaoterritorialurbanarural.model.AgrupadorDados;
import br.com.bomdestino.tributacaoterritorialurbanarural.model.DadosProprietario;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;

/**
 * Endpoint responsável por disponibilizar os serviços para geração de relatório IPTU
 *
 * @author Marcio Costa
 */
@RestController
@RequestMapping("/stur")
public class IptuController {

    @GetMapping(value = "/{iptu}/{pessoa}")
    public ResponseEntity<Object> getItpu(@PathVariable final Integer iptu,
                                          @PathVariable final String pessoa) {

        AgrupadorDados agrupadorDados = new AgrupadorDados();
        if (("J".equals(pessoa) || "j".equals(pessoa)) && iptu.compareTo(Integer.valueOf("54321")) == 0) {
            agrupadorDados.setDadosProprietarioLista(
                    Arrays.asList(getIptuPJ(iptu, pessoa)));
        } else if (("F".equals(pessoa) || "f".equals(pessoa)) && iptu.compareTo(Integer.valueOf("12345")) == 0) {
            agrupadorDados.setDadosProprietarioLista(
                    Arrays.asList(getIptuPF(iptu, pessoa)));
        }
        return new ResponseEntity<Object>(agrupadorDados, HttpStatus.OK);
    }

    private DadosProprietario getIptuPF(final Integer iptu, final String tipoPessoa) {
        return DadosProprietario.builder()
                .identificacao(iptu)
                .enderecoImovel("Rua Cinco de Maio, 123")
                .proprietario("João das Couves")
                .valor(Double.valueOf("399.99"))
                .tipoPessoa(tipoPessoa)
                .build();
    }

    private DadosProprietario getIptuPJ(final Integer iptu, final String tipoPessoa) {
        return DadosProprietario.builder()
                .identificacao(iptu)
                .enderecoImovel("Avenida Principal, 1090")
                .proprietario("Oficina Vinte LTDA")
                .valor(Double.valueOf("809.89"))
                .tipoPessoa(tipoPessoa)
                .build();
    }
}
