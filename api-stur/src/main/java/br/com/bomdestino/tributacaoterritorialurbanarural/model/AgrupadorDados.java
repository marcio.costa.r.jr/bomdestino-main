package br.com.bomdestino.tributacaoterritorialurbanarural.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class AgrupadorDados {

    @JsonProperty("iptu")
    private List<DadosProprietario> dadosProprietarioLista;
}
