package br.com.bomdestino.tributacaoterritorialurbanarural.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class DadosProprietario {

    private Integer identificacao;
    private String proprietario;
    private String enderecoImovel;
    private Double valor;
    private String tipoPessoa;

}
