package br.com.bomdestino.informacoesMunicipaisGeorreferenciadas.controller;

import java.util.List;

/**
 * Controller para prover informações relacionadas ao senso levantado através da infraestrutura
 * de dados espaciais de Belo Horizonte com identificação POP_DOMIC_BAIRRO_2010
 *
 * @author Marcio Costa
 */
public interface IIdeBHController {

    /**
     * Retorna a lista dos dados espaciais dos bairros de Belo Horizonte
     *
     * @return {@link List<br.com.bomdestino.informacoesMunicipaisGeorreferenciadas.model.Ide>}
     */
    Object listarIde();

    /**
     * Retorna o objeto identificador dos dados espaciais de Belo Horizonte
     *
     * @param idIde identificador do IDE
     * @return {@link br.com.bomdestino.informacoesMunicipaisGeorreferenciadas.model.Ide}
     */
    Object buscaPoIdIde(final String idIde);

    /**
     * Retorna o bairro dos dados espaciais de Belo Horizonte
     *
     * @param nomeBairro nome do bairro
     * @return {@link br.com.bomdestino.informacoesMunicipaisGeorreferenciadas.model.Propriedades}
     */
    Object buscaPorBairro(final String nomeBairro);
}
