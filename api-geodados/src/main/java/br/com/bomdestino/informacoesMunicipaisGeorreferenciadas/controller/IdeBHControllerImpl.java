package br.com.bomdestino.informacoesMunicipaisGeorreferenciadas.controller;

import br.com.bomdestino.informacoesMunicipaisGeorreferenciadas.service.IIdeBHService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Endpoint responsável por disponibilizar os serviços obtidos através de consulta da IDE-BH
 *
 * @author Marcio Costa
 */
@RestController
@RequestMapping("/geodados")
public class IdeBHControllerImpl implements IIdeBHController {

    private IIdeBHService iIdeBHService;

    public IdeBHControllerImpl(final IIdeBHService iIdeBHService) {
        this.iIdeBHService = iIdeBHService;
    }

    @Override
    @GetMapping("/ide-bh")
    public Object listarIde() {
        return this.iIdeBHService.buscarInfoIde();
    }

    @Override
    @GetMapping("/ide-bh/{idIde}")
    public Object buscaPoIdIde(final @PathVariable String idIde) {
        return this.iIdeBHService.buscaPoIdIde(idIde);
    }

    @Override
    @GetMapping("/bairro/{nomeBairro}")
    public Object buscaPorBairro(final @PathVariable String nomeBairro) {
        return this.iIdeBHService.buscaPorBairro(nomeBairro);
    }
}
