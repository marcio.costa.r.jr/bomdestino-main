package br.com.bomdestino.informacoesMunicipaisGeorreferenciadas.model;

import br.com.bomdestino.informacoesMunicipaisGeorreferenciadas.controller.CatalogoApi;
import com.fasterxml.jackson.annotation.JsonClassDescription;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * Classe responsável por representar informações das propriedades da IDE
 *
 * @author Marcio Costa
 */
@Getter
@Setter
@Builder
public class Propriedade {

    @JsonProperty("ID_POP_DMC")
    private Integer id;

    @JsonProperty("NUM_BAIRRO")
    private Integer numeroBairro;

    @JsonProperty("NOME_BAIRRO")
    private String nomeBairro;

    @JsonProperty("POPULACAO")
    private Integer populacao;

    @JsonProperty("DOMICILIO")
    private Integer domicilio;

    @JsonProperty("HAB_DOM")
    private Double habPorDomicilio;

    @JsonProperty("HAB_KM2")
    private Double habPorKm;

    @JsonProperty("AREA_KM2")
    private Double areaPorKm;
}
