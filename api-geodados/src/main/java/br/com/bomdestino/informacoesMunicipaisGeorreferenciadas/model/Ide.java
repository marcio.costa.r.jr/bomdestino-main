package br.com.bomdestino.informacoesMunicipaisGeorreferenciadas.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * Classe responsável por representar informações da infraestutura de dados espaciais
 *
 * @author Marcio Costa
 */
@Getter
@Setter
@Builder
public class Ide {


    private String id;

    @JsonProperty("type")
    private String tipo;

    @JsonProperty("geometry")
    private Geometria geometria;

    @JsonProperty("properties")
    private Propriedade propriedade;
}
