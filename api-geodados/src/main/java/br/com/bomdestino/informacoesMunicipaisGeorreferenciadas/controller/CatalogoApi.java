package br.com.bomdestino.informacoesMunicipaisGeorreferenciadas.controller;

/**
 * Classe abstrata responsável para disponibilizar endpoints de api externa
 */
public abstract class CatalogoApi {

    public static String ENDERECO_IBGE = "https://servicodados.ibge.gov.br/api/v1/localidades/";

    public static String ENDERECO_IBGE_UF = ENDERECO_IBGE.concat("estados");
    public static String ENDERECO_IBGE_UF_ID = ENDERECO_IBGE_UF.concat("/");

    public static String ENDERECO_IBGE_MUNICIPIO_ID = ENDERECO_IBGE.concat("/geodados/");

    public static String ENDERECO_IDE_BH = "https://geoservicos.pbh.gov.br/geoserver/" +
            "wfs?service=WFS&version=1.0.0&request=GetFeature&typeName=ide_bhgeo:POP_DOMIC_BAIRRO_2010" +
            "&srsName=EPSG:4326&outputFormat=json";

    /** PROPRIEDADES DA GEOMETRIA **/
    public static String KEY_TYPE = "type";
    public static String KEY_COORDINATES = "coordinates";

    /** PROPRIEDADES DO IDE **/
    public static String KEY_ID_POP_DMC = "ID_POP_DMC";
    public static String KEY_NUM_BAIRRO = "NUM_BAIRRO";
    public static String KEY_NOME_BAIRRO = "NOME_BAIRRO";
    public static String KEY_POPULACAO = "POPULACAO";
    public static String KEY_DOMICILIO = "DOMICILIO";
    public static String KEY_HAB_DOM = "HAB_DOM";
    public static String KEY_HAB_KM2 = "HAB_KM2";
    public static String KEY_AREA_KM2 = "AREA_KM2";
}
