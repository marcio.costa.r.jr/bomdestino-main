import React, { useState } from 'react';

import logo from '../img/logo_geodados.png';

import '../App.css';
import '../Geodados.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Navbar from 'react-bootstrap/Navbar';
import Button from 'react-bootstrap/Button';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Popover from 'react-bootstrap/Popover';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';
import Spinner from 'react-bootstrap/Spinner';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faRuler, faVectorSquare } from '@fortawesome/free-solid-svg-icons';

import Map from "./Map";
import { Layers, TileLayer, VectorLayer } from "./Map/Layers";
import { Icon, Fill, Stroke, Style } from 'ol/style';
import { osm, vector } from "./Map/Source";
import { fromLonLat, get } from 'ol/proj';
import GeoJSON from 'ol/format/GeoJSON';
import { Controls, ZoomControl } from "./Map/Controls";

import {Link} from "react-router-dom";

let styles = {
	'MultiPolygon': new Style({
		stroke: new Stroke({
			color: 'blue',
			width: 1,
		}),
		fill: new Fill({
			color: 'rgba(0, 0, 255, 0.5)',
		}),
    }),
    'MultiPolygon2': new Style({
		stroke: new Stroke({
			color: 'green',
			width: 1,
		}),
		fill: new Fill({
			color: 'rgba(0, 128, 0, 0.5)',
		}),
	}),
};

let geoResponse = {
    "populacao": {
        "type": "FeatureCollection",
        "features": []
    },
    "ocupacao": {
        "type": "FeatureCollection",
        "features": []
    }
};

const GeodadosContainer = () => {
  let [appReady, setAppReady] = useState(false);
  const [showLayer1, setShowLayer1] = useState(false);
  const [showLayerPopulacao, setShowLayerPopulacao] = useState(true);
  const [showSearchModal, setShowSearchModal] = useState(false);
  const handleCloseSearchModal = () => setShowSearchModal(false);
  const handleShowSearchModal = () => setShowSearchModal(true);

  const axios = require('axios');

	axios.get("http://localhost:8091/geodados/ide-bh", {withCredentials: true, headers: {"Access-Control-Allow-Origin": "*"}})
    .then(function (response) {
        let mockOcupacao = [{
            "type": "Feature",
            "properties": {
            },
            "geometry": {
                "type": "MultiPolygon",
                "coordinates": [
                    [
                        [
                            [-43.9262, -19.873],
                            [-43.9253, -19.874],
                            [-43.9244, -19.8751],
                            [-43.9238, -19.8759],
                            [-43.9237, -19.8761],
                            [-43.9229, -19.8758],
                            [-43.9224, -19.8758],
                            [-43.9222, -19.8759],
                            [-43.922, -19.876],
                            [-43.9219, -19.876],
                            [-43.9219, -19.876],
                            [-43.9219, -19.8761],
                            [-43.9217, -19.8762],
                            [-43.9206, -19.8766],
                            [-43.9204, -19.8764],
                            [-43.9204, -19.8764],
                            [-43.9204, -19.8764],
                            [-43.9203, -19.8765],
                            [-43.9205, -19.8766],
                            [-43.9205, -19.8767],
                            [-43.9203, -19.8768],
                            [-43.92, -19.877],
                            [-43.9197, -19.8773],
                            [-43.9198, -19.8773],
                            [-43.9195, -19.8775],
                            [-43.9195, -19.8776],
                            [-43.9195, -19.8776],
                            [-43.9193, -19.8778],
                            [-43.9194, -19.8779],
                            [-43.9191, -19.8782],
                            [-43.9191, -19.8783],
                            [-43.9191, -19.8783],
                            [-43.9189, -19.8786],
                            [-43.919, -19.8786],
                            [-43.919, -19.8786],
                            [-43.9192, -19.8787],
                            [-43.9189, -19.8792],
                            [-43.9189, -19.8793],
                            [-43.9184, -19.88],
                            [-43.9182, -19.8802],
                            [-43.9178, -19.8805],
                            [-43.9176, -19.8807],
                            [-43.9172, -19.8809],
                            [-43.9167, -19.881],
                            [-43.9165, -19.8811],
                            [-43.9159, -19.8812],
                            [-43.9155, -19.8814],
                            [-43.9153, -19.8814],
                            [-43.9147, -19.8817],
                            [-43.9144, -19.8818],
                            [-43.9138, -19.8809],
                            [-43.9131, -19.8799],
                            [-43.9122, -19.879],
                            [-43.9117, -19.8784],
                            [-43.9118, -19.8784],
                            [-43.912, -19.8783],
                            [-43.9125, -19.8782],
                            [-43.9131, -19.8781],
                            [-43.9133, -19.878],
                            [-43.9134, -19.878],
                            [-43.9135, -19.8779],
                            [-43.9137, -19.8778],
                            [-43.9143, -19.8775],
                            [-43.9141, -19.8771],
                            [-43.9145, -19.8765],
                            [-43.9146, -19.8763],
                            [-43.9149, -19.8763],
                            [-43.9151, -19.8763],
                            [-43.9153, -19.8763],
                            [-43.9154, -19.8763],
                            [-43.9157, -19.8761],
                            [-43.9157, -19.876],
                            [-43.9162, -19.8755],
                            [-43.9165, -19.8752],
                            [-43.9166, -19.8751],
                            [-43.9167, -19.875],
                            [-43.9174, -19.8748],
                            [-43.9181, -19.8746],
                            [-43.9183, -19.8746],
                            [-43.9188, -19.8746],
                            [-43.9193, -19.8746],
                            [-43.9194, -19.8745],
                            [-43.9199, -19.8743],
                            [-43.9205, -19.8741],
                            [-43.9206, -19.874],
                            [-43.9208, -19.8739],
                            [-43.921, -19.8737],
                            [-43.9213, -19.8734],
                            [-43.9214, -19.8732],
                            [-43.9215, -19.8731],
                            [-43.9217, -19.873],
                            [-43.9218, -19.8729],
                            [-43.922, -19.8728],
                            [-43.9224, -19.8726],
                            [-43.9226, -19.8725],
                            [-43.923, -19.8725],
                            [-43.9237, -19.8723],
                            [-43.9238, -19.8722],
                            [-43.9239, -19.8721],
                            [-43.9241, -19.8719],
                            [-43.9243, -19.872],
                            [-43.9243, -19.8722],
                            [-43.9245, -19.8722],
                            [-43.9248, -19.8724],
                            [-43.9243, -19.8729],
                            [-43.9246, -19.8732],
                            [-43.9255, -19.8725],
                            [-43.9262, -19.873]
                        ]
                    ]
                ]
            }
        },
        {
            "type": "Feature",
            "geometry": {
                "type": "MultiPolygon",
                "coordinates": [
                    [
                        [
                            [-43.9253, -19.8649],
                            [-43.925, -19.8649],
                            [-43.9248, -19.865],
                            [-43.9247, -19.865],
                            [-43.9244, -19.866],
                            [-43.9241, -19.867],
                            [-43.9238, -19.8669],
                            [-43.9235, -19.8668],
                            [-43.9228, -19.8666],
                            [-43.9228, -19.8667],
                            [-43.9227, -19.867],
                            [-43.9224, -19.8682],
                            [-43.9223, -19.8682],
                            [-43.9223, -19.8682],
                            [-43.9223, -19.8683],
                            [-43.9222, -19.8685],
                            [-43.9218, -19.8685],
                            [-43.9216, -19.8682],
                            [-43.9216, -19.8681],
                            [-43.9215, -19.868],
                            [-43.9213, -19.8679],
                            [-43.9212, -19.8678],
                            [-43.921, -19.8677],
                            [-43.921, -19.8675],
                            [-43.921, -19.8675],
                            [-43.9209, -19.8674],
                            [-43.9209, -19.8675],
                            [-43.9208, -19.8674],
                            [-43.9208, -19.8674],
                            [-43.9207, -19.8674],
                            [-43.9205, -19.8673],
                            [-43.9206, -19.8672],
                            [-43.9204, -19.8671],
                            [-43.9204, -19.8671],
                            [-43.9204, -19.867],
                            [-43.9205, -19.867],
                            [-43.9205, -19.867],
                            [-43.9205, -19.8669],
                            [-43.9206, -19.8667],
                            [-43.9209, -19.8669],
                            [-43.9209, -19.8668],
                            [-43.921, -19.8669],
                            [-43.921, -19.8669],
                            [-43.9212, -19.867],
                            [-43.9212, -19.867],
                            [-43.9212, -19.867],
                            [-43.9212, -19.8671],
                            [-43.9215, -19.867],
                            [-43.9216, -19.8669],
                            [-43.9221, -19.8665],
                            [-43.9222, -19.8664],
                            [-43.9224, -19.8663],
                            [-43.9227, -19.8659],
                            [-43.9228, -19.8657],
                            [-43.9247, -19.8643],
                            [-43.9251, -19.864],
                            [-43.9251, -19.8638],
                            [-43.9253, -19.8639],
                            [-43.9256, -19.864],
                            [-43.9256, -19.864],
                            [-43.9255, -19.8641],
                            [-43.9254, -19.8646],
                            [-43.9254, -19.8648],
                            [-43.9253, -19.8649]
                        ]
                    ]
                ]
            },
            "properties": {
            }
        }];

        geoResponse.populacao.features = response.data.features;
        geoResponse.ocupacao.features = mockOcupacao;

        setAppReady(true);
    })
    .catch(function (error) {
        console.log(error);
    });

	return (
        <>
        {appReady
        ?   <Container className="p-0" fluid>
                <Row>
                <Col>
                    <Navbar className="App-header navbar-dark" expand="lg">
                    <Navbar.Brand href="#">
                        <img src={logo} className="App-logo" alt="logo" />
                    </Navbar.Brand>
                    <Navbar.Toggle variant="secondary" aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse className="justify-content-end">
                        <Navbar.Text>
                            <Button className="mr-1" onClick={handleShowSearchModal}>Pesquisar</Button>
                            <OverlayTrigger
                                trigger="click"
                                placement={'bottom'}
                                overlay={
                                <Popover id={'popover-layers'}>
                                    <Popover.Content>
                                        <Form>
                                        <fieldset>
                                            <Form.Group as={Row}>
                                            <Form.Label as="legend" className="font-weight-bold" column sm={12}>
                                                Selecione uma camada para visualizar:
                                            </Form.Label>
                                            <Col sm={12}>
                                                <Form.Check
                                                label="População Bairros 2010"
                                                id="mapLayerOne"
                                                checked={showLayerPopulacao}
                                                onChange={event => setShowLayerPopulacao(event.target.checked)}
                                                />
                                                <Form.Check
                                                label="Ocupação - Lotes Vagos (2017)"
                                                id="mapLayerTwo"
                                                checked={showLayer1}
                                                onChange={event => setShowLayer1(event.target.checked)}
                                                />
                                                <Form.Check
                                                label="Ocupação - Indústria (2017)"
                                                id="mapLayerTree"
                                                />
                                                <Form.Check
                                                label="Ocupação - Galeria/Shopping (2017)"
                                                id="mapLayerTree"
                                                />
                                            </Col>
                                            </Form.Group>
                                        </fieldset>
                                        </Form>
                                    </Popover.Content>
                                    </Popover>
                                }
                            >
                            <Button>Gerenciar Camadas</Button>
                            </OverlayTrigger>
                        </Navbar.Text>
                        <Link className="navOption" to="/">&lt;&lt;Menu Principal</Link>
                    </Navbar.Collapse>
                    </Navbar>
                </Col>
                </Row>
                <Row>
                <Col>
                <Map center={fromLonLat([-43.9237,-19.8761])} zoom={12}>
                    <Layers>
                        <TileLayer
                            source={osm()}
                            zIndex={0}
                        />
                        {showLayer1 && (
                            <VectorLayer
                                source={vector({ features: new GeoJSON().readFeatures(geoResponse.ocupacao, { featureProjection: get('EPSG:3857') }) })}
                                style={styles.MultiPolygon2}
                            />
                        )}
                        {showLayerPopulacao && (
                            <VectorLayer
                                source={vector({ features: new GeoJSON().readFeatures(geoResponse.populacao, { featureProjection: get('EPSG:3857') }) })}
                                style={styles.MultiPolygon}
                            />
                        )}
                    </Layers>
                    <Controls>
                        <ZoomControl />
                    </Controls>
                </Map>			
                </Col>
                </Row>
                <Button id="rulerButtom" title="Medir Distância" className="mapButtons" variant="primary"><FontAwesomeIcon icon={faRuler} /></Button>
                <Button id="areaButtom" title="Medir Área" className="mapButtons" variant="primary"><FontAwesomeIcon icon={faVectorSquare} /></Button>
                <Modal show={showSearchModal} onHide={handleCloseSearchModal}>
                    <Modal.Header closeButton>
                        <Modal.Title>Pesquisa por Bairro</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                    <Form>
                        <Form.Group controlId="exampleForm.SelectCustom">
                            <Form.Label>Selecione o bairro:</Form.Label>
                            <Form.Control as="select" custom>
                                <option value="10">Dom Joaquim</option>
                                <option value="11">Vila São Paulo</option>
                                <option value="8">São Marcos</option>
                                <option value="9">Urca</option>
                                <option value="6">Nova Floresta</option>                            
                            </Form.Control>
                        </Form.Group>
                        </Form>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleCloseSearchModal}>
                            Cancelar
                        </Button>
                        <Button variant="primary" onClick={handleCloseSearchModal}>
                            Pesquisar
                        </Button>
                    </Modal.Footer>
                </Modal>
            </Container>
        :   <Container id="loadingContainer" className="spinner-container" fluid>
                <Spinner className="centerVertically" animation="border" role="status"></Spinner>
                <span className="bsPrefix">Carregando...</span>
            </Container>
        }
        </>
	);
}

export default GeodadosContainer;