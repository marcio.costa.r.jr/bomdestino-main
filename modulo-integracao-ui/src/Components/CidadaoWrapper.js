import Iptu from './Iptu';

function CidadaoWrapper() {  
  return (
      // O router controlaria qual página exibir - IPTU está fixo pois é a unica implementada nessa POC
     <Iptu></Iptu>
  );
}

export default CidadaoWrapper;
