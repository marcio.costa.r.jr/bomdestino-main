import Controls from "./Controls";
import ZoomControl from "./ZoomControl";

export {
	Controls,
	ZoomControl
}