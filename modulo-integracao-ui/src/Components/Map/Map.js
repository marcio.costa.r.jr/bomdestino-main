import React, { useRef, useState, useEffect } from "react"
import "./Map.css";
import MapContext from "./MapContext";
import {click} from 'ol/events/condition';
import Select from 'ol/interaction/Select';
import * as ol from "ol";

const Map = ({ children, zoom, center }) => {
	const mapRef = useRef();
	const [map, setMap] = useState(null);

	// on component mount
	useEffect(() => {
		const container = document.getElementById('mapPopup');
		const content = document.getElementById('popupContent');
		const closer = document.getElementById('popup-close');

		const overlay = new ol.Overlay({
			element: container
		});

		let options = {
			view: new ol.View({ zoom, center }),
			layers: [],
			controls: [],
			overlays: [overlay]
		};

		let mapObject = new ol.Map(options);
		mapObject.setTarget(mapRef.current);
		setMap(mapObject);

		/**
		 * Add a click handler to hide the popup.
		 */
		closer.onclick = function () {
			overlay.setPosition(undefined);
			closer.blur();
			return false;
		};

		/**
		 * Add a click handler to display the popup.
		 */
		mapObject.on('click', function(evt) {
			let feature = mapObject.forEachFeatureAtPixel(evt.pixel, function(feature, layer) {
				return feature;
			});

			if (feature	&& feature.values_	&& feature.values_.NOME_BAIRRO) {
				let selectClick = new Select({
					condition: click,
				});

				mapObject.addInteraction(selectClick);

				let coordinate = evt.coordinate;
				
				const popupContent = '<ul><li><span class="font-weight-bold">Bairro:</span> ' + feature.values_.NOME_BAIRRO + '</li>'
									+ '<li><span class="font-weight-bold">Nº Bairro:</span> ' + feature.values_.NUM_BAIRRO + '</li>'
									+ '<li><span class="font-weight-bold">População:</span> ' + feature.values_.POPULACAO + '</li>'
									+ '<li><span class="font-weight-bold">Habitantes por Km²:</span> ' + feature.values_.HAB_KM2 + '</li>'
									+ '<li><span class="font-weight-bold">Habitantes por Domicílio:</span> ' + feature.values_.HAB_DOM + '</li>'
									+ '<li><span class="font-weight-bold">Domicílios:</span> ' + feature.values_.DOMICILIO + '</li>'
									+ '<li><span class="font-weight-bold">Área Km²:</span> ' + feature.values_.AREA_KM2 + '</li></ul>';

				content.innerHTML = popupContent;
				overlay.setPosition(coordinate);
			}
		});

		return () => mapObject.setTarget(undefined);
	}, []);

	// zoom change handler
	useEffect(() => {
		if (!map) return;

		map.getView().setZoom(zoom);
	}, [zoom]);

	// center change handler
	useEffect(() => {
		if (!map) return;

		map.getView().setCenter(center)
	}, [center])

	return (
		<MapContext.Provider value={{ map }}>
			<div ref={mapRef} className="ol-map">
				{children}
			</div>
			<div id="mapPopup" className="map-popup">
				<button id="popup-close" className="map-popup-close" type="button" title="Fechar">x</button>
                <div id="popupContent"></div>
            </div>
		</MapContext.Provider>
	)
}

export default Map;