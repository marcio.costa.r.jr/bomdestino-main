import React from 'react';

import '../App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import CidadaoHeader from './CidadaoHeader';
import CidadaoWrapper from './CidadaoWrapper';

function CidadaoContainer() {  
  return (
    <Container className="p-0" fluid>
      <Row>
      <Col>
        <CidadaoHeader></CidadaoHeader>
      </Col>
      </Row>
      <Row>
      <Col>
        <CidadaoWrapper></CidadaoWrapper>
      </Col>
      </Row>
    </Container>
  );
}

export default CidadaoContainer;
