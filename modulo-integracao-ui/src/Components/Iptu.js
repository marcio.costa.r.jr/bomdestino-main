import React, { useState } from 'react';

import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Table from 'react-bootstrap/Table';
import Spinner from 'react-bootstrap/Spinner';
import Modal from 'react-bootstrap/Modal';
    
function Iptu() {
  const [modalEmptyShow, setModalEmptyShow] = useState(false);
  const [modalNotFoundShow, setModalNotFoundShow] = useState(false);
  const [isIptuReturned, setIsIptuReturned] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [tipoPessoa, setTipoPessoa] = useState("f");
  const [idImovel, setIdImovel] = useState("");
  const [tipoPessoaResponse, setTipoPessoaResponse] = useState("");
  const [idImovelResponse, setIdImovelResponse] = useState("");
  const [nomePessoaResponse, setNomePessoaResponse] = useState("f");
  const [valorResponse, setValorResponse] = useState("f");
  const [enderecoResponse, setEnderecoResponse] = useState("f");

  let tiposPessoa = {'f': 'Pessoa Física', 'j': 'Pessoa Jurídica'};

  let handleTipoPessoaChange = (evt) => {
    setTipoPessoa(evt.target.value);
  }

  let handleIdImovelChange = (evt) => {
    setIdImovel(evt.target.value);
  }

  let handleNovaConsulta = (evt) => {
    setIsIptuReturned(false)
  }

  let handleConsulta = (evt) => {
    if (idImovel.trim(" ") === "") {
      setModalEmptyShow(true);
      return;
    }

    setIsLoading(true);

	  const axios = require('axios');

    axios.get("http://localhost:8091/servicocidadao/iptu/" + idImovel + "/tipo/" + tipoPessoa, {headers: {"Access-Control-Allow-Origin": "*"}})
    .then(function (response) {
      if (!response.data.iptu) {
       setModalNotFoundShow(true);
       setIsLoading(false);
       return;
      }

      setIsLoading(false);
      setIsIptuReturned(true);

      let dadosImovelPesquisado = response.data.iptu[0];

      setTipoPessoaResponse(dadosImovelPesquisado.tipoPessoa);
      setIdImovelResponse(dadosImovelPesquisado.identificacao);
      setNomePessoaResponse(dadosImovelPesquisado.proprietario);
      setValorResponse(dadosImovelPesquisado.valor);
      setEnderecoResponse(dadosImovelPesquisado.enderecoImovel);
    })
    .catch(function (error) {
        setIsLoading(false);
        console.log(error);
    });
  }

  return (
    <>
    <Row>
      <Col>
        <h2 className="text-center my-4">Emissão de Guia para Pagamento do IPTU</h2>
      </Col>
    </Row>
    {!isIptuReturned
    ? <Row>
        <Col></Col>
        <Col xs={10} md={8}>
          <Form>
            <Form.Group>
              <Form.Label>Tipo de Pessoa:</Form.Label>
              <Form.Control as="select" value={tipoPessoa} onChange={handleTipoPessoaChange}>
                <option value="f">Pessoa Física</option>
                <option value="j">Pessoa Jurídica</option>
              </Form.Control>
            </Form.Group>
            <Form.Group>
              <Form.Label>Número da identificação do Imóvel:</Form.Label>
              <Form.Control
                type="text"
                placeholder="Número da identificação do Imóvel" 
                value={idImovel} onChange={handleIdImovelChange}
              />
            </Form.Group>
          </Form>
          <Button type="submit" onClick={handleConsulta}>Consultar</Button>
          {isLoading ? <Spinner className="ml-3" animation="border" variant="primary"></Spinner> : ""}
          <Modal
            size="lg"
            show={modalEmptyShow}
            onHide={() => setModalEmptyShow(false)}
          >
            <Modal.Header closeButton>
              <Modal.Title id="modal-empty-id">
                Atenção!
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>É necessário informar o Número da identificação do Imóvel!</Modal.Body>
          </Modal>
          <Modal
            size="lg"
            show={modalNotFoundShow}
            onHide={() => setModalNotFoundShow(false)}
          >
            <Modal.Header closeButton>
              <Modal.Title id="modal-not-found-id">
                Atenção!
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>Nenhum registro foi encontrado. Tente novamente!</Modal.Body>
          </Modal>
        </Col>
        <Col></Col>
      </Row>  
    : <Row>
        <Container>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>Identificação do Imóvel</th>
                <th>Tipo de Pessoa</th>
                <th>Proprietário</th>
                <th>Endereço Imóvel</th>
                <th>Valor IPTU</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>{idImovelResponse}</td>
                <td>{tiposPessoa[tipoPessoaResponse]}</td>
                <td>{nomePessoaResponse}</td>
                <td>{enderecoResponse}</td>
                <td>R$ {valorResponse}</td>
              </tr>
            </tbody>
          </Table>
          <Button variant="primary" className="mr-2" onClick={handleNovaConsulta}>Consultar outro Imóvel</Button>
          <Button variant="success">Baixar Boleto</Button>
        </Container>
      </Row>
    }
  </>
  );
}

export default Iptu;
