import React, { useState } from 'react';

import logo from '../img/logo_serv_cidadao.png';
import './Header.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import {Link} from "react-router-dom";

function CidadaoHeader() {  
  return (
    <>
      <Navbar className="App-header navbar-dark" expand="lg">
      <Navbar.Brand href="#">
        <img src={logo} className="App-logo" alt="logo" />
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse className="justify-content-end">
          <Nav className="hasVerticalDivider">
            <Nav.Link href="iptu" className="nav-link active">Consultar IPTU</Nav.Link>
            <Nav.Link href="itr">Consultar ITR</Nav.Link>
            <Nav.Link href="cvvi">Consultar Fluxos Municipais</Nav.Link>
            
          </Nav>
          <Link className="navOption" to="/">&lt;&lt;Menu Principal</Link>
        </Navbar.Collapse>
      </Navbar>
    </>
  );
}

export default CidadaoHeader;
