import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMapMarkedAlt, faUserFriends, faChartLine, faCogs, faHandHoldingUsd, faMoneyCheckAlt, faProcedures, faChalkboardTeacher } from '@fortawesome/free-solid-svg-icons';
import {Link} from "react-router-dom";

function Wrapper() {  
  return (
    <Row className="m-3">
      <Col className="text-center  mb-4" xs="12" sm="6" lg="3">
        <Card style={{ minHeight: '13rem', backgroundColor: '#eee' }}>
          <Card.Body>
            <Card.Title className="appIcon"><FontAwesomeIcon size="5x" icon={faUserFriends} /></Card.Title>
            <Card.Subtitle className="mt-2 text-muted">
              <Link to="/cidadao">Módulo de Serviços ao Cidadão</Link>
            </Card.Subtitle>
          </Card.Body>
        </Card>        
      </Col>
      <Col className="text-center  mb-4" xs="12" sm="6" lg="3">
        <Card style={{ minHeight: '13rem', backgroundColor: '#eee' }}>
          <Card.Body>
            <Card.Title className="appIcon"><FontAwesomeIcon size="5x" icon={faMapMarkedAlt} /></Card.Title>
            <Card.Subtitle className="mt-2 text-muted">
              <Link to="/geodados">Módulo de Informações Municipais Georreferenciadas</Link>
            </Card.Subtitle>
          </Card.Body>
        </Card>
      </Col>
      <Col className="text-center  mb-4" xs="12" sm="6" lg="3">  
        <Card style={{ minHeight: '13rem', backgroundColor: '#eee' }}>
          <Card.Body>
            <Card.Title className="appIcon"><FontAwesomeIcon size="5x" icon={faChartLine} /></Card.Title>
            <Card.Subtitle className="mt-2 text-muted">
              <a target="_blank" href="/businessinteligence">Módulo de Business Inteligence</a>
            </Card.Subtitle>
          </Card.Body>
        </Card>        
      </Col>
      <Col className="text-center  mb-4" xs="12" sm="6" lg="3">
        <Card style={{ minHeight: '13rem', backgroundColor: '#eee' }}>
          <Card.Body>
            <Card.Title className="appIcon"><FontAwesomeIcon size="5x" icon={faCogs} /></Card.Title>
            <Card.Subtitle className="mt-2 text-muted">
              <a target="_blank" href="https://trello.com">Módulo de Gestão Estratégica de Projetos</a>
            </Card.Subtitle>
          </Card.Body>
        </Card>
      </Col>
      <Col className="text-center  mb-4" xs="12" sm="6" lg="3">
        <Card style={{ minHeight: '13rem', backgroundColor: '#eee' }}>
          <Card.Body>
            <Card.Title className="appIcon"><FontAwesomeIcon size="5x" icon={faHandHoldingUsd} /></Card.Title>
            <Card.Subtitle className="mt-2 text-muted">
              <a target="_blank" href="/safim">SAFIM - Sistema Administrativo-Financeiro de Gestão Municipal</a>
            </Card.Subtitle>
          </Card.Body>
        </Card>
      </Col>
      <Col className="text-center  mb-4" xs="12" sm="6" lg="3">
        <Card style={{ minHeight: '13rem', backgroundColor: '#eee' }}>
          <Card.Body>
            <Card.Title className="appIcon"><FontAwesomeIcon size="5x" icon={faMoneyCheckAlt} /></Card.Title>
            <Card.Subtitle className="mt-2 text-muted">
              <a target="_blank" className="appLink" href="/stur">STUR - Sistema de Tributação Territorial Urbana e Rural</a>
            </Card.Subtitle>
          </Card.Body>
        </Card>
      </Col>
      <Col className="text-center  mb-4" xs="12" sm="6" lg="3">
        <Card style={{ minHeight: '13rem', backgroundColor: '#eee' }}>
          <Card.Body>
            <Card.Title className="appIcon"><FontAwesomeIcon size="5x" icon={faProcedures} /></Card.Title>
            <Card.Subtitle className="mt-2 text-muted">
              <a target="_blank" className="appLink" href="/sasc">SASC - Sistema de Atenção à Saúde do Cidadão</a>
            </Card.Subtitle>
          </Card.Body>
        </Card>
      </Col>
      <Col className="text-center mb-4" xs="12" sm="6" lg="3">
        <Card style={{ minHeight: '13rem', backgroundColor: '#eee' }}>
          <Card.Body>
            <Card.Title className="appIcon"><FontAwesomeIcon size="5x" icon={faChalkboardTeacher} /></Card.Title>
            <Card.Subtitle className="mt-2 text-muted">
              <a target="_blank" className="appLink" href="/saem">SAEM - Sistema de Administração Escolar Multiserial</a>
            </Card.Subtitle>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}

export default Wrapper;
