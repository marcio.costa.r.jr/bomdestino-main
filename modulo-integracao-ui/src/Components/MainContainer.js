import React, { useState } from 'react';

import '../App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Header from './Header';
import Wrapper from './Wrapper';

function MainContainer() {  
  return (
    <Container className="p-0" fluid>
      <Row>
      <Col>
        <Header></Header>
      </Col>
      </Row>
      <Row>
      <Col>
        <Wrapper></Wrapper>
      </Col>
      </Row>
    </Container>
  );
}

export default MainContainer;
