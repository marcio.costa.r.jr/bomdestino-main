import React, { useState } from 'react';

import logo from '../img/logo.png';
import './Header.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

function Header() {  
  return (
      <>
        <Navbar className="App-header navbar-dark" expand="lg">
        <Navbar.Brand href="#">
            <img src={logo} className="App-logo" alt="logo" />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse className="justify-content-end">
            <Nav className="hasVerticalDivider">
            <Nav.Link href="/logout">Sair</Nav.Link>
            </Nav>
        </Navbar.Collapse>
        </Navbar>
    </>
  );
}

export default Header;
