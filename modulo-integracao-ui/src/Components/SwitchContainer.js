import React from 'react';
import {Switch, Route} from "react-router-dom";
import MainContainer from './MainContainer';
import CidadaoContainer from './CidadaoContainer';
import GeodadosContainer from './GeodadosContainer';

function SwitchContainer() {  
  return (
    <div>
      <Switch>
        <Route exact path="/" children={<MainContainer />} />
        <Route path="/cidadao" children={<CidadaoContainer />} />
        <Route path="/geodados" children={<GeodadosContainer />} />
      </Switch>
    </div>
  );
}

export default SwitchContainer;
