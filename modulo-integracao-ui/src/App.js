import React from 'react';

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {BrowserRouter as Router} from "react-router-dom";
import SwitchContainer from './Components/SwitchContainer';

function App() {  
  return (
    <Router>
      <SwitchContainer/>
    </Router>
  );
}

export default App;
