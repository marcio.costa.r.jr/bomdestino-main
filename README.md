# Bomdestino Main

Centralizador dos módulos presentes no contexto de aplicações para a POC referente a cidade Bom Destino

Módulo Geodados
http://localhost:8091/geodados/ide-bh


Módulo de Serviços  ao Cidadão
http://localhost:8091/servicocidadao/12345/tipo/f


Módulo STUR
http://localhost:8091/stur/12345/f


Link do Diagrama de Componentes:
https://gitlab.com/marcio.costa.r.jr/bomdestino-main/-/raw/master/Diagrama%20Componentes.jpg

Link do Diagrama de Implantação:
https://gitlab.com/marcio.costa.r.jr/bomdestino-main/-/raw/master/Diagrama%20de%20Implanta%C3%A7%C3%A3o.jpg