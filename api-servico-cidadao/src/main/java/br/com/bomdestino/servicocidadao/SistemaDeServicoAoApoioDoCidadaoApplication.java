package br.com.bomdestino.servicocidadao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class SistemaDeServicoAoApoioDoCidadaoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SistemaDeServicoAoApoioDoCidadaoApplication.class, args);
	}

}
