package br.com.bomdestino.servicocidadao.controller;

import org.springframework.http.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * Endpoint responsável por disponibilizar os serviços para consultar API STUR
 *
 * @author Marcio Costa
 */
@RestController
@RequestMapping("/servicocidadao")
public class ApoioCidadaoController {

    private static final String ENDERECO_STUR = "http://api-stur:8098/stur/";

    @GetMapping(value = "/iptu/{iptu}/tipo/{tipoPessoa}")
    public ResponseEntity<Object> getIptu(@PathVariable final Integer iptu,
                                            @PathVariable final String tipoPessoa){

        final String url = ENDERECO_STUR.concat("/" + iptu.toString()).concat("/" + tipoPessoa);

        final HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(MediaType.APPLICATION_JSON);

        return new RestTemplate().exchange(url, HttpMethod.GET, new HttpEntity<>(responseHeaders),
                Object.class);
    }
}
